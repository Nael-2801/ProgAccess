<?php $tr=array (
  '_' => 'art_list',
  '_todo_level' => 0,
  '_last_author' => 'Corentin',
  '_last_modif' => 1609230733,
  'title' => 'Liste des articles',
  'sort_label' => 'Rrier par&nbsp;:',
  'sort_article_id' => 'Numéro d\'article',
  'sort_alpha_order' => 'Ordre alphabétique',
  'sort_date' => 'Date de mise à jour',
  'sort_btn' => 'Trier',
  'nb_found' => '<b>{{count}}</b> articles trouvés',
); ?>
