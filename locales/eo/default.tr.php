<?php $tr=array (
  '_' => 'default',
  '_todo_level' => 4,
  '_last_author' => 'Pascal',
  '_last_modif' => 1562492882,
  'slogan' => 'Alirebleco ĉie, de ĉiuj, por ĉiuj',
  'banner_homelink' => 'Ĉefpaĝo',
  'fndate' => '%Y-%m-%d',
  'ftdate' => '%A %e-a de %B %Y',
  'ftime' => '%H:%M',
  'fstime' => '%H:%M',
  'fndatetime' => '%Y-%m-%d T %H:%M',
  'ftdatetime' => '%A %e-a de %B %Y T %H:%M',
  'loginbox_profilelink' => 'Profilo',
  'loginbox_alistlink' => 'Listo de membroj',
  'loginbox_adminlink' => 'Administracio',
  'loginbox_logoutlink' => 'Elsaluti',
  'loginbox_memberarea' => 'Mia konto',
  'loginbox_loginlabel' => 'Ensaluti',
  'loginbox_username' => 'Uzantnomo',
  'loginbox_password' => 'Pasvorto',
  'loginbox_forgotpsw' => 'Pasvorto forgesita',
  'loginbox_signup' => 'Krei konton',
  'loginbox_notifs' => 'Atentigoj',
  'loginbox_notifs_sg' => '{{n}} atentigo',
  'loginbox_notifs_pl' => '{{n}} atentigoj',
  'loginbox_notifs_readall' => 'Ĉiujn marki kiel legita',
  'loginbox_notifs_read' => 'Marki kiel legita',
  'searchtool_label' => 'Serĉi',
  'searchtool_text' => 'Serĉi programaron/instruilon',
  'searchtool_cat' => 'Kategorio',
  'searchtool_all' => 'Tuto',
  'urank_0' => 'Nova',
  'urank_1' => 'Membro',
  'urank_a' => 'Membro de la teamo',
  'urank_i' => 'anonyma',
  'footer_share' => 'Komunigi ĉi retpaĝo kun:',
  'footer_fb' => 'Facebook',
  'footer_tw' => 'Twitter',
  'footer_g+' => 'Google+',
  'footer_d*' => 'Diaspora*',
  'footer_lastmodif' => 'Lasta modifo',
  'footer_d-1' => 'hieraŭ',
  'footer_d-2' => 'antaŭhieraŭ',
  'footer_today' => 'hodiaŭ',
  'footer_ontime' => 'la',
  'footer_devver' => 'Versio dev',
  'footer_changelog' => 'Konsulti la ŝanĝoj de ĉi beta versio',
  'footer_copyright' => '{{site}} (<a href="https://txmn.tk/eo/blog/why-copyleft/">Kial rajtocedo?</a>)',
  'footer_license' => 'la <a href="/opensource.php">kodo de {{site}}</a> estas disponebla laŭ la permesilo {{license}}.<br>
La teksto estas disponebla laŭ la permesilo {{trlicense}}.',
  'footer_opensource' => 'pri la libera kodo',
  'footer_stablelink' => 'Stabila zono (prod)',
  'footer_devlink' => 'Zono dev',
  'footer_toplink' => 'Paĝosupro',
  'footer_menulink' => 'Menuo',
  'footer_ctnlink' => 'Ĉefa enhavo',
  'menu_changelang' => 'Ŝanĝi la lingvo',
  'menu_articles' => 'Programaroj/instruiloj',
  'menu_news' => 'Sekvi la actualaĵo',
  'menu_nl' => 'Novaĵletero',
  'menu_rss' => 'Abonfluo RSS',
  'menu_soon' => 'Baldaŭ',
  'menu_usefull' => 'Iloj',
  'menu_forum' => 'Forumo',
  'menu_pocket' => '{{site}} en via poŝo',
  'menu_contest' => 'Konkurso',
  'menu_sets' => 'Parametroj',
  'menu_gadgets' => 'Akcesoraĵoj kaj servoj',
  'menu_infos' => 'Pri {{site}}',
  'menu_contact' => 'Kontakti nin',
  'menu_menutitle' => 'Menuo',
  'menu_ctnlink' => 'Ĉefa enhavo',
  'menu_bottomlink' => 'Paĝofundo',
  'menu_searchlink' => 'Serĉi',
  'menu_linklistlabel' => 'Iri al:',
  'menu_homepage' => 'Ĉefpaĝo',
  'menu_switchmenu' => 'Kaŝi/montri la menuon',
  'menu_toplink' => 'Paĝosupro',
  'decimal_separator' => ',',
  'byte_letter' => 'B',
  'menu_privacy' => 'Privateco',
  'urank_b' => 'Ekzilita',
  'menu_journal' => 'Lastaj ŝanĝoj',
  'footer_link_g1' => '{{site}} kaj libera mono Ğ1',
  'menu_support' => 'Helpi',
  'menu_reco' => 'Rekomendi la retejon',
  'footer_link_d*' => '{{site}} sur Diaspora*',
  'footer_link_tw' => '{{site}} sur Twitter',
  'footer_link_fb' => '{{site}} sur Facebook',
  'loginbox_adminhome' => 'Ĉefpaĝo de la administracio',
  'menu_linklistlabelbutton' => 'konfirmi',
  'urank_r' => 'Redaktisto',
  'urank_s' => 'Redaktisto privilegiita',
  'urank_t' => 'Traduktisto',
); ?>
